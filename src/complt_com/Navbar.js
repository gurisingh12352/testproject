import React from 'react'
import logo from '../img/Group.png';
export default function Navbar() {
  return (<nav>
      <img src={logo} alt='logo'/>
      <h1>ReactApp</h1>
      <ul>
        <li>Home</li>
        <li>Pricing</li>
        <li>About</li>
      </ul>
  </nav>
  )
}
