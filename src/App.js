import './App.css';
import './styles/style.css';
import MainFrame from './complt_com/mainframe';

function App() {
  return (
    <div className="App">
    <MainFrame />
    </div>
  );  
}

export default App;
